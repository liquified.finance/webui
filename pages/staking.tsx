import {useContext, useEffect, useState} from "react";
import Nav from "../components/nav";
import StakingPool, {IStakingPoolProps} from "../components/stakingPool";
import {ethers} from "ethers";
import {useWeb3React} from "@web3-react/core";
import {ERC20ABI, joeLPAddress, pangoLPAdress, TokenABI, TokenAdress} from "../components/consts";
import {GlobalModalContext, IGlobalModalContext} from "../components/globalModal";
import NetworkWrapper from "../components/networkWrapper";
import {tryTransact} from "../components/util";
import Button from "../components/simple_button"
import Input from "../components/simple_input"

function Staking() {
    const [userPools, setUserPools] = useState<string[]>()

    useEffect(() => {
        if (userPools)
            localStorage.setItem("UserPools", JSON.stringify(userPools))
    }, [userPools])

    useEffect(() => {
        if (!userPools)
            setUserPools(JSON.parse(localStorage.getItem("UserPools")) ?? [])
    }, [])

    const hardcodedPools: IStakingPoolProps[] = [
        {
            reward: joeLPAddress,
            name: 'Joe LP (LQFD/AVAX)'
        },
        {
            reward: pangoLPAdress,
            name: 'Pangolin LP (LQFD/AVAX)'
        }
    ]
    const {account, library} = useWeb3React();
    const pools = [...hardcodedPools, ...((userPools ?? []).map(r => ({
        reward: r, onDelete: () => {
            setUserPools(userPools.filter((R) => R != r))
        }
    })))]
    const [input, setInput] = useState<string>("")

    const tokenContract = new ethers.Contract(
        TokenAdress,
        TokenABI,
        library?.getUncheckedSigner(),
    );

    const modalContext: IGlobalModalContext = useContext(GlobalModalContext)

    const onRegister = async () => {
        if (validateInput()) {
            modalContext.pushMessage(validateInput())
            return
        }
        if (input.length == 0) {
            modalContext.pushMessage("Address empty!")
            return
        }
        const tx = tokenContract.registerLP(input)
        tryTransact(tx, modalContext.pushMessage)
    }

    const onAdd = () => {
        if (validateInput()) {
            modalContext.pushMessage(validateInput())
            return
        }
        if (input.length == 0) {
            modalContext.pushMessage("Address empty!")
            return
        }
        if (userPools && !userPools.includes(input))
            setUserPools([...userPools, input])
    }

    const handleInput = (event: any) => {
        const {value} = event.target;
        setInput(value);
    };

    const validateInput = () => {
        if (input.length == 0) {
            return null
        }
        try {
            ethers.utils.getAddress(input)
        } catch (e) {return "Invalid address";}
        return null;
    }

    return (
        <>
            <Nav isLanding={false} currentId="staking"></Nav>
            <main className="min-h-screen text-white">
                <p className="m-4 text-center text-7xl">Stake LQFD for community liquidity!</p>
                <p className="m-4 text-center text-7xl">Staking pools:</p>
                <div className="flex flex-wrap items-start content-start m-10">
                    {pools.map((p) => <div className="inline-block m-2 w-[28rem]"><StakingPool {...p} key={p.reward} /></div>)}
                    <div className="w-[28rem] inline-block m-2 p-4 bg-blue-600 rounded-md space-y-2">
                        <Input
                            className="w-full"
                            placeholder="Address"
                            type="text"
                            value={input}
                            onChange={handleInput}
                        />
                        <p className="text-red-600">{validateInput()}</p>
                        <div className="inline-block space-x-2">
                            <Button onClick={onAdd}>
                                Add pool
                            </Button>
                            <Button onClick={onRegister}>
                                Register LP
                            </Button>
                        </div>
                    </div>
                </div>
            </main>
        </>
    );
}

export default function StakingWrapper() {
    return <NetworkWrapper>
        <Staking />
    </NetworkWrapper>
}
