import MigrationBox from "@components/migration_box";
import NetworkWrapper from "@components/networkWrapper";
import Nav from "@components/nav";
import {useWeb3React} from "@web3-react/core";
import {joeLPAddress, joeOPMNLPAddress, MigrationABI, MigrationAddress, OPMNAddress, pangoLPAdress, pangoOPMNLPAddress, TokenAdress} from "@components/consts";
import {ethers} from "ethers";
import useEtherSWR from "ether-swr";

export default function Migration() {
    const {account, library} = useWeb3React()
    const migrationContract = new ethers.Contract(
        MigrationAddress,
        MigrationABI,
        library?.getUncheckedSigner(),
    );

    return <NetworkWrapper>
        <Nav isLanding={false} currentId="migration"></Nav>
        <main className="min-h-screen p-4 text-white space-y-4">
            <p className="content-center justify-center m-4 text-center text-7xl">Migrate OPMN and its liquidity to LQFD</p>
            <div className="flex flex-col items-center justify-center inline-block p-10 m-0 lg:flex-row lg:space-x-2 lg:space-y-0 space-y-2">
                <MigrationBox migrator={migrationContract.address} migrationFunction={migrationContract.migrateOPMN} inputToken={OPMNAddress} outputToken={TokenAdress} inputSymbol="OPMN" outputSymbol="LQFD"/>
                <MigrationBox migrator={migrationContract.address} migrationFunction={migrationContract.migratePangoLP} inputToken={pangoOPMNLPAddress} outputToken={pangoLPAdress} inputSymbol="OPMN/AVAX PLP" outputSymbol="LQFD/AVAX PLP"/>
                <MigrationBox migrator={migrationContract.address} migrationFunction={migrationContract.migrateJoeLP} inputToken={joeOPMNLPAddress} outputToken={joeLPAddress} inputSymbol="OPMN/AVAX JLP" outputSymbol="LQFD/AVAX JLP"/>
            </div>
        </main>
    </NetworkWrapper>;
}
