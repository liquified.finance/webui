import {AppProps} from "next/app";
import {DefaultSeo} from "next-seo";
import "../styles/globals.css";
import {Web3ReactProvider} from "@web3-react/core";
import {Web3Provider} from "@ethersproject/providers";
import GlobalModal from "../components/globalModal";
import Footer from "@components/footer";

function getLibrary(provider: any) {
    const library = new Web3Provider(provider);
    library.pollingInterval = 12000;
    return library;
}

function MyApp({Component, pageProps}: AppProps) {
    return (
        <>
            <DefaultSeo
                openGraph={{
                    type: "website",
                    url: "https://liquified.finance/",
                    title: "Liquified.finance",
                    locale: "en_US",
                    images: [
                        {
                            url: "https://liquified.finance/logotext.png",
                        },
                    ],
                }}
                title="Liquified.finance"
                description="Innovative, trustless yield generating platform on Avax C-Chain"
            />
            <div className="bg-black">
                <GlobalModal>
                    <Web3ReactProvider getLibrary={getLibrary}>
                        <Component {...pageProps} />
                        <Footer/>
                    </Web3ReactProvider>
                </GlobalModal>
            </div>
        </>
    );
}

export default MyApp;
