import {useWeb3React} from "@web3-react/core";
import {BigNumber, ethers} from "ethers";
import {useContext, useEffect, useState} from "react";
import {ERC20ABI, maxint256, StakingABI, StakingAddress, TokenAdress} from "./consts";
import {bigNumberToInt, printBigNumber, tryTransact} from "./util";
import ReactModal from 'react-modal';
import {PoolUpdateType} from './poolUpdateDialog'
import PoolUpdateDialog from './poolUpdateDialog'
import {GlobalModalContext, IGlobalModalContext} from "./globalModal";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTimes, faAngleUp, faAngleDown} from "@fortawesome/free-solid-svg-icons";
import Button from './simple_button'


export interface IStakingPoolProps {
    reward: string,
    name?: string,
    onDelete?: () => void,
}

export interface PoolData {
    rewardName: string
    rewardSymbol: string
    LQFDBalanceAccount: BigNumber
    LQFDAllowance: BigNumber
    stakeAccount: BigNumber
    stakeTotal: BigNumber
    rewardAvailable: BigNumber
    rewardPending: BigNumber
}


export default function StakingPool(props: IStakingPoolProps) {

    const {account, library, chainId} = useWeb3React();
    const [poolData, setPoolData] = useState<PoolData>(null);
    const [dialog, setDialog] = useState<PoolUpdateType>(null);
    const [expanded, setExpanded] = useState<boolean>(false);


    const stakingContract = new ethers.Contract(
        StakingAddress,
        StakingABI,
        library?.getUncheckedSigner(),
    );

    const tokenContract = new ethers.Contract(
        TokenAdress,
        ERC20ABI,
        library?.getUncheckedSigner(),
    );

    const setState = () => {
        (async function () {
            const data = await stakingContract.poolUserInfo(props.reward, account);
            setPoolData(data);
        })();
    }

    useEffect(() => {
        {
            setState();
            library.on("block", setState);
        }
    }, [account, chainId, library]);

    const modalContext: IGlobalModalContext = useContext(GlobalModalContext)

    const onStake = (value: BigNumber) => {
        setDialog(null)
        const tx = stakingContract.deposit(props.reward, value);
        tryTransact(tx, modalContext.pushMessage)
    }

    const onApprove = () => {
        setDialog(null)
        const tx = tokenContract.approve(StakingAddress, maxint256);
        tryTransact(tx, modalContext.pushMessage)

    }

    const onWithdraw = (value: BigNumber) => {
        setDialog(null)
        const tx = stakingContract.withdraw(props.reward, value);
        tryTransact(tx, modalContext.pushMessage)
    }

    const onFlip = () => {
        setExpanded(!expanded)
    }

    var content

    if (poolData) {
        const cUserStake = <p className="text-2xl"> Your stake: {printBigNumber(poolData.stakeAccount)} LQFD </p>
        const cTotalStake = <p className="text-2xl"> Total stake: {printBigNumber(poolData.stakeTotal)} LQFD </p>
        const cAvailReward = <p className="text-2xl"> Available reward: {printBigNumber(poolData.rewardAvailable)} {poolData.rewardSymbol} </p>
        const cPendingReward = <p className="text-2xl"> Pending reward: {printBigNumber(poolData.rewardPending)} {poolData.rewardSymbol} </p>
        const cEstimate =
            poolData.stakeTotal.gt(BigNumber.from(0)) ?
                <p className="text-2xl">
                    Est. reward after 24h: {printBigNumber(poolData.rewardAvailable.mul(poolData.stakeAccount).div(poolData.stakeTotal).div(BigNumber.from(50)))} {poolData.rewardSymbol}
                </p>
                : null
        content = expanded ? [cUserStake, cTotalStake, cAvailReward, cPendingReward, cEstimate] : [cUserStake, cPendingReward]
    }




    return <>
        {dialog != null &&
            <PoolUpdateDialog
                dType={dialog}
                poolData={poolData}
                onClose={() => {setDialog(null)}}
                onStake={onStake}
                onWithdraw={onWithdraw}
                onApprove={onApprove}
            />
        }
        <div className="p-4 bg-blue-600 rounded-md">
            <div className="text-3xl">

                <button className="float-right p-2 text-3xl rounded-md focus:outline-none" onClick={onFlip}>
                    <FontAwesomeIcon icon={expanded ? faAngleUp : faAngleDown} />
                </button>

                {props.name ?? poolData?.rewardName ?? "[Loading...]"}
                {!!props.onDelete ?
                    <button className="float-right p-2 text-3xl text-red-600 rounded-md focus:outline-none" onClick={props.onDelete}>
                        <FontAwesomeIcon icon={faTimes} onClick={props.onDelete} />
                    </button>
                    : null
                }
            </div>

            <p className="text-xs">
                ({props.reward})
            </p>
            <br />
            {content}
            <br />
            <div className="space-x-2">
                <Button onClick={() => setDialog(PoolUpdateType.Deposit)}>
                    Deposit
                </Button>
                <Button onClick={() => setDialog(PoolUpdateType.Withdraw)}>
                    Withdraw
                </Button>
                <Button onClick={() => onWithdraw(BigNumber.from(0))}>
                    Harvest
                </Button>

            </div>
        </div>
    </>
}
