import Image from "next/image";

export default function Logo() {
    return (
        <a className="flex flex-row items-center text-3xl space-x-3" href="#">
            <Image
                src="/logo.svg"
                width="64"
                height="64"
                layout="fixed"
            ></Image>
            <span>Liquified.finance</span>
        </a>
    );
}
