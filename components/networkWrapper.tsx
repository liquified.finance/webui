import {useWeb3React} from "@web3-react/core"
import {ReactNode, useEffect} from "react"
import {ChainID} from "./consts"
import {Web3Provider} from "@ethersproject/providers"
import {Connector} from './util'
import {useRouter} from 'next/router';
import ReactModal from 'react-modal'
import Nav from "./nav"


export interface INetworkWrapperProps {
    children: ReactNode
}

export default function NetworkWrapper(props: INetworkWrapperProps) {
    const {account, library, chainId, active, activate} = useWeb3React<Web3Provider>()
    const router = useRouter();
    const okay = !!account && !!library && chainId == ChainID;
    useEffect(() => {
        if (!active)
            activate(Connector)
    }, [account]);
    useEffect(() => {
        if (!okay) {
            // router.push({
            //     pathname: '/',
            // });
        }
    }, [okay]);
    if (okay) {
        return <>
            {props.children}
        </>
    }
    return <>
    <Nav/>
<main className="min-h-screen p-4 text-white space-y-4">
            <p className="content-center m-4 text-center text-7xl">Error: no web3 connection</p>
            <div className="flex justify-center inline-block space-x-3">
                Contents of this page will not be displayed without proper web3 connection.
            </div>
        </main>



    <div className="h-screen">
        <span className="text-2xl text-white">
        </span>
    </div>
    {
        }
</>
}
