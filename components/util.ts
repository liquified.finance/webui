import {BigNumber} from "@ethersproject/bignumber";
import {Web3Provider} from "@ethersproject/providers";
import {formatEther} from "@ethersproject/units";
import {useWeb3React} from "@web3-react/core";
import {InjectedConnector} from "@web3-react/injected-connector";
import {ChainID} from "./consts";

export const Connector = new InjectedConnector({
    supportedChainIds: [ChainID],
});

export function printBigNumber(bn: BigNumber) {
    if(!bn)
        return
    const str = formatEther(bn)
    const i = str.indexOf(".")
    return str.slice(0, i + 3)
}

export const showAddr = (addr: string) =>
    addr.slice(0, 6) + "..." + addr.slice(addr.length - 5, addr.length - 1);

export function bigNumberToInt(bn: BigNumber) {
    const bnstr = bn.toString()
    return parseInt(bnstr.slice(0, bnstr.length - 18))
}

export function isValidNumber(value: string) {
    const allowedCharachters = "1234567890.";
    return allowedCharachters.indexOf(value[value.length - 1]) !== -1 ||
        value.length === 0

}
export async function tryTransact(tx: Promise<any>, pushMessage: (msg: string) => void) {
    try {
        await tx
    }
    catch (e) {
        if (e?.data?.message != null)
            pushMessage(e.data.message)
        else if(e.code==4001)
            pushMessage("Transaction cancelled!")
        else
            pushMessage(e.message)
    }
}

export function useNetworkState() {
    const {account, library, chainId,} = useWeb3React<Web3Provider>()
    const okay = !!account && !!library && chainId == ChainID;
    return okay
}
