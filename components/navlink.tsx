import Link from "next/link";

interface INavLinkProps {
    title: string;
    href: string;
    selected: boolean;
    disabled: boolean;
}

export default function NavLink(props: INavLinkProps) {
    const underline = props.selected ? "underline" : "";

    if (props.disabled)
        return (
            <a className={`text-gray-400 py-2 md:px-1`}>
                {props.title}
            </a>
        )
    return (
        <Link href={props.href}>
            <a className={`hover:underline ${underline} py-2 md:px-1`}>
                {props.title}
            </a>
        </Link>
    );
}
