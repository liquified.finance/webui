import {Web3Provider} from "@ethersproject/providers";
import {UnsupportedChainIdError, useWeb3React} from "@web3-react/core";
import {BigNumber, ethers} from "ethers";
import {useEffect, useState} from "react";
import {useLocalStorage} from "react-use";
import Balance from "./balance";
import ConnectButton from "./connectbutton";
import {TokenAdress, TokenABI} from "./consts";
import {Connector, printBigNumber, showAddr} from "./util";

export default function Account() {
    const {account, library, chainId, active, activate} =
        useWeb3React<Web3Provider>();

    const [hasConnected] = useLocalStorage("hasConnected", false);
    const [balance, setBalance] = useState<BigNumber>(null);
    const [isUnsupportedNetwork, setIsUnsupportedNetwork] = useState(false);

    const setState = () => {
        setIsUnsupportedNetwork(false);

        const erc20 = new ethers.Contract(TokenAdress, TokenABI, library);

        erc20.balanceOf(account).then((b) => {
            setBalance(b);
        });
    };

    useEffect(() => {
        const handleError = (e: Error) => {
            if (e instanceof UnsupportedChainIdError) {
                setIsUnsupportedNetwork(true);
            }
        };

        if (hasConnected && !active) {
            activate(Connector, handleError, false);
        }
    }, []);

    useEffect(() => {
        if (library) {
            setState();
            library.on("block", setState);
        }
    }, [account, library, chainId]);

    return !isUnsupportedNetwork ? (
        active && library ? (
            <>
                <Balance
                    balance={balance !== null ? printBigNumber(balance) : ""}
                    adress={showAddr(account)}
                />
            </>
        ) : (
            <ConnectButton />
        )
    ) : (
        <span>unsupported network</span>
    );
}
