import Particles from "react-particles-js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import Logo from "./logo";
import Image from "next/image";
import TgButton from "./tgbutton";

export default function Stars() {
    return (
        <div className="justify-center min-h-screen grid grid-rows-3">
            <div className="absolute z-0">
                <Particles
                    params={{
                        particles: {
                            number: {
                                value: 160,
                                density: {
                                    enable: true,
                                    value_area: 800,
                                },
                            },
                            shape: {
                                type: "circle",
                            },
                            size: {
                                value: 3,
                                random: true,
                            },
                            opacity: {
                                value: 1,
                                random: true,
                            },
                            line_linked: {
                                enable: false,
                            },
                        },
                        interactivity: {
                            events: {
                                onhover: {
                                    enable: true,
                                    mode: "repulse",
                                },
                                onclick: {
                                    enable: false,
                                },
                            },
                        },
                    }}
                    width="97vw"
                    height="100vh"
                />
            </div>
            <div className="grid justify-items-center">
                <Image
                    src="/logo.svg"
                    alt="OpenMoon.finance"
                    width="256"
                    height="256"
                    layout="fixed"
                ></Image>
            </div>
            <div className="z-10 flex flex-col justify-center text-3xl text-center text-white md:text-5xl max-w-screen-2xl">
                <h1>
                    Innovative, trustless yield generating platform on Avax
                    C-Chain
                </h1>
            </div>
            <div className="z-10 flex flex-col justify-center text-3xl text-center text-white md:text-5xl">
                <span>
                    <FontAwesomeIcon icon={faArrowDown} />
                </span>
            </div>
        </div>
    );
}
