export default (props: any) =>
<input
        {...props}
        className={`px-1 text-2xl text-black placeholder-gray-700 bg-blue-400 border-4 border-black rounded-md focus:outline-none ${props.className}`}
        type="text"
    />;
