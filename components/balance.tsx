export interface IBalanceProps {
    adress: string;
    balance: string;
}

export default function Balance(props: IBalanceProps) {
    return (
        <>
            <div className="flex items-center justify-center px-2 m-1 border-4 border-white flew-row space-x-3 rounded-md">
                <span>{props.balance} LQFD</span>
            </div>
            <div className="flex items-center justify-center px-2 border-4 border-white flew-row rounded-md m-1">
                <span>{props.adress}</span>
            </div>
        </>
    );
}
