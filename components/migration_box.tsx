import {useWeb3React} from '@web3-react/core'
import useEtherSWR from 'ether-swr';
import {BigNumber, ethers} from 'ethers';
import {useContext, useState} from 'react';
import {ERC20ABI, maxint256} from './consts';
import {GlobalModalContext, IGlobalModalContext} from './globalModal';
import MaxableInput from './maxableinput';
import Button from './simple_button'
import Input from './simple_input'
import {isValidNumber, printBigNumber, tryTransact} from './util';

export interface IMigrationBoxProps {
    migrator: string;
    inputToken: string;
    inputSymbol: string;
    outputToken: string;
    outputSymbol: string;
    migrationFunction: (amount: any) => any;
}

export default function MigrationBox(props: IMigrationBoxProps) {
    const {account, library} = useWeb3React()
    const [input, setInput] = useState<string>();
    const modalContext: IGlobalModalContext = useContext(GlobalModalContext)

    const inputTokenContract = new ethers.Contract(
        props.inputToken,
        ERC20ABI,
        library?.getUncheckedSigner(),
    );

    const outputTokenContract = new ethers.Contract(
        props.outputToken,
        ERC20ABI,
        library?.getUncheckedSigner(),
    );

    const {data: allowance} = useEtherSWR([account, inputTokenContract.address, props.migrator], () => inputTokenContract.allowance(account, props.migrator))
    const {data: inputBalance} = useEtherSWR([inputTokenContract.address, "balanceOf", account], () => inputTokenContract.balanceOf(account))
    const {data: outputBalance} = useEtherSWR([outputTokenContract.address, "balanceOf", account], () => outputTokenContract.balanceOf(account))

    const handleInput = (event: any) => {
        const {value} = event.target;
        if (isValidNumber(value)
        ) {
            setInput(value);
        }
    };

    const allowed = !!allowance && !!inputBalance && allowance.gt(inputBalance)
    // const allowed = true
    // const allowed = false

    var value: BigNumber

    try {
        value = ethers.utils.parseEther(input);
    } catch {
        value = null;
    }

    const onMax = () => {
        setInput(ethers.utils.formatEther(inputBalance))
    }

    const onApprove = () => {
        const tx = inputTokenContract.approve(props.migrator, maxint256)
        tryTransact(tx, modalContext.pushMessage)
    }

    const onMigrate = () => {
        const tx = props.migrationFunction(value)
        tryTransact(tx, modalContext.pushMessage)
    }

    const error = (() => {
        if (value == null) {
            if (input)
                return "Invalid number!"
            return
        }
        if (inputBalance.lt(value)) {
            return "Selected amount exceeds your balance."
        }
        return null;
    })()





    return <div className="flex flex-col h-10 p-4 bg-blue-600 w-96 rounded-md space-y-2 h-72">
        <span className="text-2xl">
            Migrate {props.inputSymbol} to {props.outputSymbol}
        </span>
        { allowed ?
        <>
            <span className="h-full"/>
            <div className="flex space-x-2">
                <Input placeholder="Amount" className="w-full" value={input} onChange={handleInput} />
                <Button onClick={onMax}>
                    Max
                </Button>
            </div>
            <p className="text-red-600">
                {error}
            </p>
            <Button onClick={onMigrate}>
                Migrate
            </Button>
            <div>
                Your balance:
                <br/>
                <span className="text-red-600">
                    {`${printBigNumber(inputBalance)} ${props.inputSymbol}`}
                </span>
                <br/>
                <span className="text-green-300">
                    {`${printBigNumber(outputBalance)} ${props.outputSymbol}`}
                </span>
            </div>
        </>
        :
        <>
            <span className="h-full"/>
            <Button className="self-end w-full" onClick={onApprove}>
                Approve
            </Button>
        </>
    }
    </div>
}
