import * as deploy from './deploy.json'

export const OPMNAddress ='0xD8b9F9B8dd11B9F6a1e0C7Da6a2690720FDC8Da3'
export const TokenAdress = deploy.contracts.LiquifiedToken.address
export const StakingAddress = deploy.contracts.LiquifiedStaking.address
export const MigrationAddress = deploy.contracts.OpenMoonMigrationContract.address
export const pangoLPAdress = '0x1ec9cf2bb9c46f906f55468c6b4710c06ae1b89e'
export const joeLPAddress = '0x005b572f52cc9f7d7d9d2592887371f875375a36'
export const pangoOPMNLPAddress = '0xbBD098c7a0335D4237FbDD14b91EFEd163B29E4a'
export const joeOPMNLPAddress = '0xbe67a9673935ad4ecdfdff397f944198eb868996'
export const ChainID = 43114
// export const ChainID = 31337
export const StakingABI = deploy.contracts.LiquifiedStaking.abi
export const TokenABI = deploy.contracts.LiquifiedToken.abi
export const MigrationABI = deploy.contracts.OpenMoonMigrationContract.abi
export const ERC20ABI = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"}]
export const maxint256 =  "115792089237316195423570985008687907853269984665640564039457584007913129639935"

