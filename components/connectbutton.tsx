import { useWeb3React } from "@web3-react/core";
import { Web3Provider } from "@ethersproject/providers";
import { Connector } from "./util";
import { useLocalStorage } from "react-use";

export default function ConnectButton() {
    const web3 = useWeb3React<Web3Provider>();
    const [_, setHasConnected] = useLocalStorage("hasConnected", false);

    const handleClick = () => {
        web3.activate(Connector, (e) => {}, false);
        setHasConnected(true);
    };

    return (
        <>
            <button
                className="flex flew-row items-center justify-center space-x-3 rounded-md bg-green-500 hover:bg-green-600 p-2 focus:outline-none focus:ring-none"
                onClick={handleClick}
            >
                Connect
            </button>
        </>
    );
}
