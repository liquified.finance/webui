import { ReactNode } from "react";

interface IContentSectionProps {
    title: string;
    even?: boolean;
    children: ReactNode;
}

export default function ContentSection(props: IContentSectionProps) {
    return (
        <section className={`px-6 py-20 text-white ${props.even ? "bg-[#0e0e0e]" : null}`}>
            <div className="container flex items-center mx-auto">
                <div className="flex-1">
                    <h2 className="pl-4 mb-4 text-2xl border-l-4 sm:text-3xl border-secondary">
                        {props.title}
                    </h2>
                    {props.children}
                </div>
                <div className="justify-end flex-1 hidden bg-indigo-700 lg:flex"></div>
            </div>
        </section>
    );
}
