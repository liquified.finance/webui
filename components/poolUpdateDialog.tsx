import {BigNumber, ethers} from "ethers";
import {isValidNumber} from "./util";
import ReactModal from 'react-modal';
import {PoolData} from "./stakingPool";
import {useState} from "react";
import Button from './simple_button'
import Input from './simple_input'


interface IPoolUpdateDialogProps {
    dType: PoolUpdateType,
    poolData: PoolData
    onClose: () => void,
    onStake: (amount: BigNumber) => void,
    onWithdraw: (amount: BigNumber) => void,
    onApprove: () => void,
}

export enum PoolUpdateType {
    Deposit,
    Withdraw,
}
export default function PoolUpdateDialog(props: IPoolUpdateDialogProps) {
    const [input, setInput] = useState<string>();
    const handleInput = (event: any) => {
        const {value} = event.target;
        if (isValidNumber(value)
        ) {
            setInput(value);
        }
    };
    var value: BigNumber;
    try {
        value = ethers.utils.parseEther(input);
    } catch {
        value = null;
    }
    const validateInput = () => {
        if (value == null) {
            return "Invalid number!";
        }
        if (props.dType == PoolUpdateType.Deposit && props.poolData.LQFDBalanceAccount.lt(value)) {
            return "Selected amount exceeds your account's current balance.";
        }
        if (props.dType == PoolUpdateType.Withdraw && props.poolData.stakeAccount.lt(value)) {
            return "Selected amount exceeds your account's current stake.";
        }
        return null;
    }

    const onMax = () => {
        switch (props.dType) {
            case PoolUpdateType.Withdraw:
                setInput(ethers.utils.formatEther(props.poolData.stakeAccount))
                break
            case PoolUpdateType.Deposit:
                setInput(ethers.utils.formatEther(props.poolData.LQFDBalanceAccount))
                break
        }

    }
    return <ReactModal
        isOpen={props.dType != null}
        onRequestClose={props.onClose}
        data={{tabindex: null}}
        ariaHideApp={false}
        className="m-10 focus:outline-none"
    >
        <div
            className="h-auto max-w-lg p-4 m-auto text-white bg-blue-600 border-4 border-black rounded-md space-y-2">
            <span className="text-2xl text-center">
                {props.dType == PoolUpdateType.Deposit ?
                    "Stake LQFD for " + props.poolData.rewardName :
                    "Unstake LQFD from " + props.poolData.rewardName + " pool"}
            </span>
            <div className="flex space-x-2">
                <Input
                    className="w-full"
                    type="text"
                    value={input}
                    onChange={handleInput}
                />
                <Button
                    onClick={onMax}
                >
                    MAX
                </Button>
            </div>
            <p className="text-red-600">{validateInput()}</p>
            <div className="space-x-2">
                {props.dType == PoolUpdateType.Deposit ?
                    value == null || props.poolData.LQFDAllowance.gt(value) ?
                        <Button
                            onClick={() => props.onStake(value)}
                            disabled={validateInput() != null}>
                            Stake
                        </Button> :
                        <Button
                            onClick={props.onApprove}
                            disabled={validateInput() != null}>
                            Approve
                        </Button> :
                    <Button
                        onClick={() => props.onWithdraw(value)}
                        disabled={validateInput() != null}>
                        Unstake
                    </Button>}
                <Button
                    onClick={props.onClose}>
                    Cancel
                </Button>
            </div>
        </div>
    </ReactModal>
}
