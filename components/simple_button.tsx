export default function Button(props: any) {

    return <button {...props} className={`px-1 text-white text-2xl border-4 border-white rounded-md focus:outline-none focus:ring-none hover:underline disabled:border-gray-500 disabled:text-gray-500 ${props.className}`}/>
}
