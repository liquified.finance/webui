import {useState} from "react";
import {useMediaQuery} from "react-responsive";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars, faTimes} from "@fortawesome/free-solid-svg-icons";
import NavLink from "./navlink";
import TgButton from "./tgbutton";
import Logo from "./logo";
import Account from "./account";
import {useNetworkState} from "./util";

interface INavProps {
    currentId?: string;
    isLanding?: boolean;
}

interface INavLink {
    id: string;
    href: string;
    title: string;
    networkNeeded?: boolean;
}

export default function Nav(props: INavProps) {
    const [isVisible, setVisible] = useState<boolean>(false);

    const handleVisible = () => setVisible(!isVisible);

    const isMobile = useMediaQuery({query: "(max-width: 768px)"});

    const netState = useNetworkState()

    const links: Array<INavLink> = [
        {
            id: "home",
            href: "/",
            title: "Home",
        },
        // {
        //     id: "whitepaper",
        //     href: "/whitepaper.pdf",
        //     title: "Whitepaper",
        // },
        {
            id: "staking",
            href: "/staking",
            title: "Staking",
            networkNeeded: true,
        },
        {
            id: "migration",
            href: "/migration",
            title: "Migration",
            networkNeeded: true,
        }
    ];

    const linkElements = links.map((m) => {
        return (
            <NavLink
                key={m.id}
                href={m.href}
                title={m.title}
                selected={m.id === props.currentId}
                disabled={m.networkNeeded && (!netState)}
                // disabled={m.networkNeeded}
            />
        );
    });

    return (
        <header className="top-0 z-50 flex flex-row flex-wrap items-center justify-between px-6 py-6 text-white bg-blue-600 md:space-x-4">
            <Logo />
            <button
                onClick={handleVisible}
                className="inline-block w-8 h-8 p-1 focus:outline-none focus:ring-none md:hidden"
            >
                {isVisible ? (
                    <FontAwesomeIcon icon={faTimes} size="lg" />
                ) : (
                    <FontAwesomeIcon icon={faBars} size="lg" />
                )}
            </button>
            {!isMobile || isVisible ? (
                <nav className="flex flex-col w-full text-xl font-bold md:flex-row md:space-x-6 md:w-auto p-9 md:p-0">
                    {linkElements}
                        <Account />
                </nav>
            ) : null}
        </header>
    );
}
