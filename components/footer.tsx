import {
    faGitlab,
    faTelegramPlane,
    faTwitter,
} from "@fortawesome/free-brands-svg-icons";
import Logo from "./logo";
import SocialIcon, { ISocialIconProps } from "./socialicon";

export default function Footer() {
    const socials: Array<ISocialIconProps> = [
        {
            href: "https://t.me/LiquifiedCommunity",
            icon: faTelegramPlane,
        },
        {
            href: "https://twitter.com/LiquifiedAVAX/",
            icon: faTwitter,
        },
        {
            href: "https://gitlab.com/openmoon",
            icon: faGitlab,
        },
    ];
    return (
        <footer className="flex justify-center pb-6 mt-12 text-white flew-row flex-cols-3 space-x-10">
            <div></div>
            <div className="flex flex-col w-full max-w-screen-xl md:flex-row md:justify-between">
                <Logo />
                <div className="py-6">
                    <span>Stay connected:</span>
                    <div className="flex flex-row mt-2 space-x-2">
                        {socials.map((s) => (
                            <SocialIcon
                                key={s.href}
                                href={s.href}
                                icon={s.icon}
                            />
                        ))}
                    </div>
                </div>
            </div>
            <div></div>
        </footer>
    );
}
