import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTelegramPlane } from "@fortawesome/free-brands-svg-icons";

export default function TgButton() {
    return (
        <a
            href="https://t.me/OpenMoonCommunity"
            className="flex items-center justify-center p-2 bg-blue-600 border-4 border-white flew-row space-x-3 rounded-md hover:bg-blue-700"
        >
            <FontAwesomeIcon icon={faTelegramPlane} size="lg" />
            <span>Join Telegram</span>
        </a>
    );
}
