import {BigNumber, ethers} from "ethers";
import {useState} from "react";
import Button from "./simple_button"
import Input from "./simple_input"
import {isValidNumber} from "./util";

interface IMaxableInputProps {
    maximum;
}

export default function MaxableInput(props: IMaxableInputProps) {
    const [input, setInput] = useState<string>();

    const handleInput = (event: any) => {
        const {value} = event.target;
        if (isValidNumber(value)
        ) {
            setInput(value);
        }
    };
    var value: BigNumber;
    try {
        value = ethers.utils.parseEther(input);
    } catch {
        value = null;
    }
    const validateInput = () => {
        if (value == null) {
            return "Invalid number!";
        }
        if (props.maximum.lt(value)) {
            return "Selected amount exceeds e.";
        }
        return null;
    }

    const onMax = () => {
        setInput(ethers.utils.formatEther(props.maximum))
    }
    return
    <>
        <div className="flex space-x-2">
            <Input placeholder="Amount" className="w-full" value={input} onChange={handleInput}/>
            <Button onClick={onMax}>
                Max
            </Button>
        </div>
        <p className="mx-2 text-red-600">{validateInput()}</p>
    </>

}
