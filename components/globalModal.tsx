import {createContext, ReactNode, useState} from "react";
import ReactModal from 'react-modal'
import Button from './simple_button'

export interface IGlobalModalProps {
    children: ReactNode
}

export interface IGlobalModalContext {
    pushMessage: (msg: string) => void
}

export const GlobalModalContext = createContext<IGlobalModalContext>(null)
export default function GlobalModal(props: IGlobalModalProps) {
    const [messages, setMessages] = useState<string[]>([])
    const context: IGlobalModalContext = {
        pushMessage: (msg: string) => setMessages([...messages, msg])
    }
    const onDismiss = () => {
        const [msg, ...msgs] = messages
        setMessages(msgs)
    }
    return <GlobalModalContext.Provider value={context}>
        <ReactModal
            isOpen={messages.length != 0}
            onRequestClose={onDismiss}
            ariaHideApp={false}
            className="m-10 focus:outline-none"
        >
            <div
                className="h-auto max-w-lg p-4 m-auto text-red-600 bg-blue-600 border-4 border-black text-2xl rounded-md space-y-2">
                <p>
                    Error: {messages[0]}
                </p>
                <Button onClick={onDismiss}>
                    Ok
                </Button>


            </div>
        </ReactModal>
        {props.children}
    </GlobalModalContext.Provider>
}
